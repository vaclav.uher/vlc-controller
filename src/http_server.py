import tempfile
from os import remove
from os.path import isfile, isdir, exists
from shutil import move

from flask import Flask, render_template, request, redirect, jsonify
from flask_dropzone import Dropzone
from flask_uploads import patch_request_class
from werkzeug.utils import secure_filename

from actions import do_action, transcode_video, get_playlist
from logger import core_logger
from settings import *
from tasks import Tasks

app = Flask(__name__, template_folder=TEMPLATES_FOLDER, static_folder=STATIC_FOLDER)
dropzone = Dropzone(app)

# Dropzone settings
app.config['DROPZONE_UPLOAD_MULTIPLE'] = True
app.config['DROPZONE_ALLOWED_FILE_CUSTOM'] = True
app.config['DROPZONE_ALLOWED_FILE_TYPE'] = 'video/*'
app.config['DROPZONE_REDIRECT_VIEW'] = 'dashboard'
app.config['DROPZONE_UPLOAD_ACTION'] = 'dashboard'

actions = []


# Uploads settings
app.config['UPLOADED_FILES_DEST'] = UNUSED_FOLDER
patch_request_class(app, size=1024*1024*1024*8)  # set maximum file size, default is 8 GiB


def str_to_bool(string):
    if string.lower() in ['yes', 'true', '1', 'y']:
        return True
    else:
        return False


@app.route('/', methods=['GET'])
@app.route('/dashboard', methods=['GET'])
def dashboard():
    core_logger.info('Dashboard rendering.')
    tasks = Tasks([MUNICIPAL_RADIO, PRESENTATIONS, CLIPS, UNUSED])
    try:
        playlist = get_playlist()
    except Exception as e:
        core_logger.debug(e)
        playlist = None
    return render_template('dashboard.html',
                           clips=tasks.clips(), unused=tasks.unused(), playlist=playlist,
                           presentation_id=environ[PRESENTATION_ID], text_logo=environ['TEXT_LOGO'],
                           base_url=environ['BASE_URL'], stream_url=environ['RTSP_URL'],
                           processing=actions)


@app.route('/upload/video/<logo_handle>', methods=['POST'])
def video_upload(logo_handle=None):
    if request.method == 'POST':
        core_logger.info('Uploading video.')
        file_obj = request.files
        for file in file_obj:
            video_file = file_obj[file]
            # check if the post request has the file part
            # if user does not select file, browser also
            # submit an empty part without filename
            if video_file.filename == '':
                continue
            if video_file and allowed_file(video_file.filename):
                filename = secure_filename(video_file.filename)
                destination = join(UNUSED_FOLDER, filename)
                temp_upload = tempfile.NamedTemporaryFile()
                if exists(destination):
                    core_logger.warn('File {} already exists.')
                    continue
                core_logger.info('Saving uploaded file to {}.'.format(destination))
                video_file.save(temp_upload)
                add_logo = False
                if logo_handle == 'include_logo':
                    add_logo = True
                transcode_video(temp_upload.name, destination, add_logo)
                temp_upload.close()

    return dashboard()


@app.route('/template')
def template():
    core_logger.info('Template rendering.')
    return render_template('template.html')


@app.route('/action/<action>')
def call_action(action=None):
    actions.append(action)
    if action:
        do_action(action)
    try:
        actions.remove(action)
    except ValueError:
        core_logger.debug('Can not remove {} from list.'.format(action))
    return redirect("/dashboard", code=302)


@app.route('/ping')
def ping():
    core_logger.info('Action/ping.')
    return 'ok'


@app.route('/vlc')
def vlc():
    core_logger.info('Redirect to VLC web interface.')
    redirect_url = "http://"+request.headers['Host'].split(':')[0]+':'+str(environ[VLC_WEB_PORT])
    return redirect(redirect_url, code=302)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/file-move/<file>/<source>/<target>', methods=['GET'])
def move_file(file, source, target):
    source = source.split('-', 1)[1]
    target = target.split('-', 1)[1]
    source_file_path = join(DATA_FOLDER, source, file)
    response = {}
    if not isfile(source_file_path):
        response['status'] = 'error'
        response['reason'] = 'File does not exist.'
        return jsonify(response)
    if target == 'delete':
        remove(source_file_path)
        response['status'] = 'ok'
        response['reason'] = 'Deleted.'
        return jsonify(response)
    destination_directory = join(DATA_FOLDER, target)
    if not isdir(destination_directory):
        response['status'] = 'error'
        response['reason'] = 'Target directory does not exist.'
        return jsonify(response)
    destination_file_path = join(destination_directory, file)
    if isfile(destination_file_path):
        response['status'] = 'error'
        response['reason'] = 'Destination file already exists.'
        return jsonify(response)
    try:
        move(source_file_path, destination_file_path)
    except OSError as e:
        response['status'] = 'error'
        response['reason'] = str(e)
        return jsonify(response)
    response['status'] = 'ok'
    response['reason'] = 'Copied.'
    return jsonify(response)


def run_http(host='127.0.0.1', port=5000, debug=False):
    # generate presentation
    if not debug:
        do_action('update_presentation')
    do_action('startup')
    app.run(host=host, port=int(port), debug=debug, use_reloader=False)

    return app


if __name__ == '__main__':
    core_logger.info('Starting app.')
    run_http(environ[HTTP_HOST], environ[HTTP_PORT], debug=environ[HTTP_DEBUG] == 'yes')
