import os
from settings import DATA_FOLDER, CLIPS, UNUSED


class Tasks(list):

    def __init__(self, task_names):
        tasks = []
        for task_name in task_names:
            tasks.append(Task(task_name))
        list.__init__(self, tasks)

    def clips(self):
        return self.get_task_by_name(CLIPS)

    def unused(self):
        return self.get_task_by_name(UNUSED)

    def get_task_by_name(self, name):
        for task in self:
            if task.name == name:
                return task
        return None


class Task:
    def __init__(self, name):
        self.name = name

    def files(self):
        dir_path = os.path.join(DATA_FOLDER, self.name)
        files = os.listdir(dir_path)
        files.sort()
        return files
