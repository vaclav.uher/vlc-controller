from datetime import datetime
from os import listdir, system, mkdir
from shutil import rmtree

import requests
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from pdf2image import convert_from_path

from logger import core_logger
from settings import *
from settings import RC_HOST, RC_PORT, STATIC_FOLDER
from vlc_connector import VLCClient
from PIL import Image

TIME_INDEX = 0
COMMAND_INDEX = 2


def reload_scheduler():
    sched.remove_all_jobs()

    filepath = join(STATIC_FOLDER, 'schedule.txt')
    with open(filepath) as fp:
        line = fp.readline()
        while line:
            # skip comments
            if line.startswith('#'):
                core_logger.debug('skipping ' + line)
                line = fp.readline()
                continue
            parsed = line.rpartition(' ')
            command = parsed[COMMAND_INDEX]
            time = parsed[TIME_INDEX]
            sched.add_job(lambda: do_action(command=command), CronTrigger.from_crontab(time))
            core_logger.info('Scheduling {} at {}'.format(command, time))
            line = fp.readline()


def update_presentation():
    core_logger.info('action/update presentation.')
    presentation_id = environ[PRESENTATION_ID]
    seconds_per_slide = environ[SECONDS_PER_SLIDE]
    video_codec = environ[PRESENTATION_VIDEO_CODEC]

    core_logger.info('Updating presentation.')
    time = datetime.now().strftime('%Y%m%d%H%M%S')
    folder = join(PRESENTATIONS_FOLDER, time)
    try:
        mkdir(folder)
    except OSError as e:
        core_logger.error('Cannot create presentation directory {}'.format(folder))
        raise e

    filename = 'presentation_{}.pdf'.format(time)
    path = join(folder, filename)

    # TODO URL does not exists
    url = 'https://docs.google.com/presentation/d/{}/export/pdf'.format(presentation_id)
    core_logger.info('Downloading {}.'.format(url))
    r = requests.get(url)

    with open(path, 'wb') as f:
        f.write(r.content)
    core_logger.info('Converting pdf {} to images.'.format(path))
    images = convert_from_path(path)

    for index, image in enumerate(images):
        extrema = image.convert("L").getextrema()
        if abs(extrema[0] - extrema[1]) < 5:
            break
        else:
            image_path = join(folder, 's{:03d}.png'.format(index))
            image.thumbnail((1920, 1080), Image.ANTIALIAS)
            image.save(image_path)

    output_name = 'presentation_{}.mp4'.format(time)
    output_path = join(folder, output_name)
    # command = "ffmpeg -framerate 1/{} -i {}/s%03d.png -r 25 -vcodec {} -y {}".format(
    command = 'ffmpeg -framerate 1/{} -i {}/s%03d.png -tune stillimage -preset slow -crf 18 -r 25 -c:v {} -y {}'.format(
        seconds_per_slide,
        folder,
        video_codec,
        output_path)
    core_logger.info('FFMPEG images to video: {}'.format(command))

    system(command)

    return output_path


def transcode_video(source, destination, insert_logo=True):
    logo = ''

    if insert_logo:
        logo = ' -i {} -filter_complex "overlay={}:{}"'.format(
            join(LOGO_FOLDER, environ[LOGO_NAME]),
            environ[LOGO_X],
            environ[LOGO_Y])

    # command = 'ffmpeg -i {}{} -c:v {} -preset slow -crf 22 -c:a copy {}'.format(
    command = 'ffmpeg -i {}{} -vf scale=1920:-1 -c:v {} -c:a copy {}'.format(
        source,
        logo,
        environ[VIDEO_OUTPUT_CODEC],
        destination
    )
    core_logger.info('Converting video: {}'.format(command))
    system(command)


def audio_to_video(audio, image, destination):
    command = 'ffmpeg -loop 1 -i {} -i {} -c:v libx264 -tune stillimage -c:a aac -b:a 192k ' \
              '-pix_fmt yuv420p -shortest {}'.format(image, audio, destination)
    core_logger.info('Converting audio to video: {}.'.format(command))
    system(command)


def update_municipal_radio():
    core_logger.info('Updating municipal radio video.')
    time = datetime.now().strftime('%Y%m%d%H%M%S')
    folder = join(MUNICIPAL_RADIO_FOLDER, time)
    try:
        mkdir(folder)
    except OSError as e:
        core_logger.info('Cannot create {} directory.'.format(folder))
        raise e

    filename = 'radio_{}.mp3'.format(time)
    audio_file = join(folder, filename)

    # TODO URL does not exists
    #url = 'http://www.znc.cz/layout/files/1359204445_05%20-%20Balonek.mp3'
    url = 'https://www.mediacollege.com/audio/tone/files/1kHz_44100Hz_16bit_05sec.mp3'

    core_logger.info('Downloading audio file {}.'.format(url))
    r = requests.get(url)

    with open(audio_file, 'wb') as f:
        f.write(r.content)

    output_name = 'radio_{}.mp4'.format(time)
    output_file = join(folder, output_name)
    image_file = join(LOGO_FOLDER, environ[MUNICIPAL_RADIO_IMAGE])

    audio_to_video(audio_file, image_file, output_file)
    core_logger.info('Audio converted - {}.'.format(output_file))
    return output_file


def play_pause():
    core_logger.info('action/play_pause.')
    vlc_rc.play_pause()


def vlc_next():
    core_logger.info('action/next.')
    vlc_rc.next()


def play_municipal_radio():
    core_logger.info('action/play municipal radio.')
    files = listdir(MUNICIPAL_RADIO_FOLDER)
    files.sort()
    # TODO directory is empty
    name = files[-1]
    file = join(MUNICIPAL_RADIO_FOLDER, name)
    vlc_rc.add_directory_and_delete_rest(file)


def play_clips():
    vlc_rc.add_directory_and_delete_rest(CLIPS_FOLDER)


def play_presentation():
    core_logger.info('Action/play clips.')
    files = listdir(PRESENTATIONS_FOLDER)
    files.sort()
    # TODO directory is empty
    name = files[-1]
    file = join(PRESENTATIONS_FOLDER, name)
    vlc_rc.add_directory_and_delete_rest(file)

    if environ[DELETE_OLD_PRESENTATIONS] == 'yes':
        for presentation in files:
            if not presentation == name:
                rmtree(join(PRESENTATIONS_FOLDER, presentation), ignore_errors=True)


def get_playlist():
    return vlc_rc.playlist()


def play_municipal_radio_presentation():
    # TODO
    print('TODO')
    #play_municipal_radio()
    #vlc_rc.add()


def startup():
    if len(listdir(CLIPS_FOLDER)) > 0:
        play_clips()
    else:
        if not vlc_rc.is_playing() and vlc_rc.connected():
            vlc_rc.add(environ['VLC_DEFAULT_VIDEO_PATH'])
            vlc_rc.play()


def do_action(command):
    core_logger.info('Running {} command.'.format(command))
    if command in available_functions:
        available_functions[command]()
    else:
        core_logger.warn('Command not found: ' + command)


available_functions = {'play_clips': play_clips,
                       'play_municipal_radio': play_municipal_radio,
                       'play_municipal_radio_presentation': play_municipal_radio_presentation,
                       'play_pause': play_pause,
                       'play_presentation': play_presentation,
                       'reload_scheduler': reload_scheduler,
                       'startup': startup,
                       'update_municipal_radio': update_municipal_radio,
                       'update_presentation': update_presentation,
                       'vlc_next': vlc_next
                       }


# init telnet client
core_logger.info('initializing VLC telnet client.')
vlc_rc = VLCClient(host=environ[RC_HOST], port=environ[RC_PORT])
# TODO handle connection refused when vlc is not running

sched = BackgroundScheduler()
reload_scheduler()
core_logger.info('Starting scheduler.')
sched.start()
