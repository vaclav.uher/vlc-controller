# coding: utf-8
"""
    VLCClient
    ~~~~~~~~~

    This module allows to control a VLC instance through a python interface.

    You need to enable the RC interface, e.g. start
    VLC like this:

    $ vlc --intf telnet --telnet-password admin

    To start VLC with allowed remote admin:
    $ vlc --intf telnet --telnet-password admin \
      --lua-config "telnet={host='0.0.0.0:4212'}"

    Replace --intf with --extraintf to start telnet and the regular GUI

    More information about the telnet interface:
    http://wiki.videolan.org/Documentation:Streaming_HowTo/VLM

    :author: Michael Mayr <michael@dermitch.de>
    :licence: MIT License
    :version: 0.2.0
"""

from __future__ import print_function

from os.path import join
from os import listdir, environ
import socket
from time import sleep
from playlist import Playlist
from settings import ALLOWED_EXTENSIONS, RC_PORT, RC_HOST


class VLCClient(object):
    """
    Connection to a running VLC instance with RC interface.
    """

    def __init__(self, host=environ[RC_HOST], port=environ[RC_PORT]):
        self.server = host
        self.port = int(port)
        self.socket = None

    def connect(self):
        try:
            vlc_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            vlc_socket.connect((self.server, self.port))
            self.socket = vlc_socket
        except ConnectionRefusedError as e:
            self.socket = None
            raise e

    def connected(self):
        return self.socket is not None

    def disconnect(self):
        """
        Disconnect and close connection
        """
        self.socket.close()

    def _send_command(self, line):
        """
        Sends a command to VLC and returns the text reply.
        This command may block.
        """
        '''Prepare a command and send it to VLC'''
        if not line.endswith('\n'):
            line = line + '\n'
        line = line.encode()

        if not self.socket:
            try:
                self.connect()
            except ConnectionRefusedError as e:
                raise e

        self.socket.sendall(line)
        return self.socket.recv(4096).decode("UTF-8")

    #
    # Commands
    #
    def help(self):
        """Returns the full command reference"""
        return self._send_command("help")

    def longhelp(self):
        """Returns the full command reference"""
        return self._send_command("longhelp")

    def status(self):
        """current playlist status"""
        try:
            return self._send_command("status")
        except ConnectionRefusedError as e:
            return 'Not connected'

    def is_playing(self):
        try:
            state = self.status()
            return 'state playing' in str(state)
        except:
            return False

    def info(self):
        """information about the current stream"""
        return self._send_command("info")

    def set_fullscreen(self, value):
        """set fullscreen on or off"""
        assert type(value) is bool
        return self._send_command("fullscreen {}".format("on" if value else "off"))

    #
    # Playlist
    #
    def add(self, filename):
        """
        Add a file to the playlist and play it.
        This command always succeeds.
        """
        return self._send_command('add {0}'.format(filename))

    def enqueue(self, filename):
        """
        Add a file to the playlist. This command always succeeds.
        """
        return self._send_command("enqueue {0}".format(filename))

    def delete(self, order):
        return self._send_command('delete {}'.format(order))

    def seek(self, second):
        """
        Jump to a position at the current stream if supported.
        """
        return self._send_command("seek {0}".format(second))

    def play(self):
        """Start/Continue the current stream"""
        return self._send_command("play")

    def pause(self):
        """Pause playing"""
        return self._send_command("pause")

    def play_pause(self):
        """Play/pause action"""
        if self.is_playing():
            self.pause()
        else:
            self.play()

    def stop(self):
        """Stop stream"""
        return self._send_command("stop")

    def rewind(self):
        """Rewind stream"""
        return self._send_command("rewind")

    def next(self):
        """Play next item in playlist"""
        return self._send_command("next")

    def prev(self):
        """Play previous item in playlist"""
        return self._send_command("prev")

    def clear(self):
        """Clear all items in playlist"""
        return self._send_command("clear")

    def loop(self):
        """Toggle loop"""
        return self._send_command("loop")

    def repeat(self):
        """Toggle repeat of a single item"""
        return self._send_command("repeat")

    def random(self):
        """Toggle random playback"""
        return self._send_command("random")

    def playlist(self):
        """Get whole playlist."""
        try:
            playlist = Playlist(self._send_command('playlist'))
        except ConnectionRefusedError as e:
            raise e
        return playlist

    def delete_tracks(self, tracks):
        for track in tracks:
            self.delete(track)

    def add_and_delete_rest(self, to_add):
        old_playlist = self.playlist()
        self.add(to_add[0])
        for track in to_add[1:]:
            self.enqueue(track)
        sleep(0.5)
        self.delete_tracks(old_playlist.keys())

    def add_directory(self, directory):
        files = list(filter(lambda x: x.lower().endswith(tuple(ALLOWED_EXTENSIONS)), sorted(listdir(directory))))
        if not isinstance(files, list):
            files = [files]
        self.add(join(directory, files[0]))
        if len(files) > 1:
            for file in files[1:]:
                self.enqueue(join(directory, file))

    def add_directory_and_delete_rest(self, directory):
        old_playlist = self.playlist()
        self.add_directory(directory)
        sleep(0.5)
        self.delete_tracks(old_playlist.keys())

    #
    # Volume
    #
    def volume(self, vol=None):
        """Get the current volume or set it"""
        if vol:
            return self._send_command("volume {0}".format(vol))
        else:
            return self._send_command("volume").strip()

    def volup(self, steps=1):
        """Increase the volume"""
        return self._send_command("volup {0}".format(steps))

    def voldown(self, steps=1):
        """Decrease the volume"""
        return self._send_command("voldown {0}".format(steps))

    #
    # Custom
    #

    def is_playing_last(self):
        # TODO empty playlist, rewrite
        playlist = self.playlist()
        playlist = playlist[2:]

        # for item in playlist:

        print(playlist)
        return False


def main():

    vlc = VLCClient('localhost', 8888)
    vlc.stop()
    print(vlc.playlist())


if __name__ == '__main__':
    main()
