import logging.handlers
from logging import StreamHandler
import sys

LEVEL = logging.DEBUG

logFormatter = logging.Formatter("[%(name)s] [%(levelname)s]  %(message)s")
core_logger = logging.getLogger('VLC_stream_controller')
core_logger.setLevel(LEVEL)

#syslog_handler = logging.handlers.SysLogHandler(address='/dev/log')
syslog_handler = StreamHandler(sys.stdout)
syslog_handler.setFormatter(logFormatter)


core_logger.addHandler(syslog_handler)
core_logger.addHandler(logging.StreamHandler())
