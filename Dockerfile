FROM python:3.6-alpine
RUN apk update
RUN apk add --no-cache build-base jpeg-dev zlib-dev ffmpeg poppler-utils
ENV LIBRARY_PATH=/lib:/usr/lib
ENV PYTHONUNBUFFERED 1

COPY data/ /srv/data/
RUN mkdir /srv/data/clips
RUN mkdir /srv/data/municipal_radio
RUN mkdir /srv/data/presentations
RUN mkdir /srv/data/unused


WORKDIR /srv

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY src/ /srv/src/

VOLUME /srv/data/clips /srv/data/imgs /srv/data/municipal_radio /srv/data/presentations /srv/data/test /srv/data/unused /srv/data/static /srv/data/templates

EXPOSE 5000/tcp

CMD [ "python", "src/http_server.py" ]