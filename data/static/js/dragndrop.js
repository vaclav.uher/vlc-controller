var dragged;
var links = document.querySelectorAll("li.draggable"),
	el = null;

for (var i = 0; i < links.length; i++) {
	el = links[i];
	el.setAttribute("draggable", "true");
}

/* events fired on the draggable target */
document.addEventListener("drag", function(event) {}, false);

document.addEventListener(
	"dragstart",
	function(event) {
		// store a ref. on the dragged elem
		dragged = event.target;
		// make it half transparent
		event.target.style.opacity = 0.5;
		remove_pointer_events(event.target);
	},
	false
);

document.addEventListener(
	"dragend",
	function(event) {
		// reset the transparency
		event.target.style.opacity = "";
		enable_pointer_events();
	},
	false
);

/* events fired on the drop targets */
document.addEventListener(
	"dragover",
	function(event) {
		// prevent default to allow drop
		event.preventDefault();
	},
	false
);

document.addEventListener(
	"dragenter",
	function(event) {
		// highlight potential drop target when the draggable element enters it
		if (event.target.className == "dropzones") {
			event.target.style.background = "white";
		}
	},
	false
);

document.addEventListener(
	"dragleave",
	function(event) {
		// reset background of potential drop target when the draggable element leaves it
		if (event.target.className == "dropzones") {
			event.target.style.background = "";
		}
	},
	false
);

document.addEventListener(
	"drop",
	function (event){
    // prevent default action (open as link for some elements)
	event.preventDefault();

    // move dragged elem to the selected drop target
	if (event.target.className == "dropzones") {
	    var link = '../file-move/'+dragged.innerText+'/'+dragged.parentNode.id+'/'+event.target.id
        var url = new URL(link, window.location.href).href;
        getJSON(url,
            function(err, data, event) {
                event.target.style.background = "";
                if (err !== null || data.status != 'ok') {
                    alert('Something went wrong: ' + err);
                } else {
		            dragged.parentNode.removeChild(dragged);
		            event.target.appendChild(dragged);
                }
            }, event);
    }
},
	false
);




var getJSON = function(url, callback, event) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
    var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response, event);
      } else {
        callback(status, xhr.response, event);
      }
    };
    xhr.send();
};

function remove_pointer_events(object) {
	for (var i = 0; i < links.length; i++) {
		el = links[i];
		if (el === object) continue;
		el.style.pointerEvents = "none";
	}
}

function enable_pointer_events() {
	for (var i = 0; i < links.length; i++) {
		el = links[i];
		el.style.pointerEvents = "all";
	}
}